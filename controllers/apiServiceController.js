
var httpRequest = require('request');

var config = require('./apiConfig');
var info={
  'token':'',
  'username':''
};

module.exports = function (request,res) {
	var options = {
		url: 'https://api.instagram.com/oauth/access_token',
		method: 'POST',
    //json: true,
		form: {
			client_id: config.instagram.client_id,
			client_secret: config.instagram.client_secret,
			grant_type: 'authorization_code',
			redirect_uri: config.instagram.redirect_uri,
			code: request.query.code
		}
	};

  // POST request to instagram auth
	httpRequest(options, function (error,response, body) {

		if (!error && response.statusCode == 200) {
			var user = JSON.parse(body);

      info.token=user.access_token;
      info.username=user.user.username;
      request.session.Iinfo=info;
      request.session.save();
        res.redirect('/insta');
		}else {
		  res.redirect('/');
		}

	});

};
